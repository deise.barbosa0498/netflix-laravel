# Netflix Laravel

O Netflix Laravel tem por objetivo de estudo adaptar fluxos e rotas existentes no site oficial da Netflix à liguagem PHP com o uso do framework Laravel, para aprendizado e desenvolvimento pessoal nas seguintes questões:

- Uso correto Git Flow
- Uso de funções e recursos nativos do PHP
- Conhecimento das novas funcionalidades disponíveis no Laravel 10.
- Melhoria na organização de código e estruturação de projetos complexos.

## Tecnologias

- PHP 8.0.26
- Laravel 10.1.5

## Para executar o projeto

- Clonar projeto:

```
git clone https://gitlab.com/deise.barbosa0498/netflix-laravel.git
```

- Baixar dependências PHP:

```
composer install
```

- Renomear o arquivo .env.example >> .env, adicionar as credenciais e gerar a chave do projeto: 

```
php artisan key:generate 
```

- Rodar as migrations no banco de dados
```
php artisan migrate 
```

- Rodar o projeto localmente (rodando na porta 8000):

```
php artisan serve
```